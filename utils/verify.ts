import { run } from "hardhat"
//import { modules } from "web3"

export async function verify(contractAdress : string, args : any[]) {
    console.log("Verifying contract..")
    try {
        await run("verify:verify", {
            address: contractAdress,
            constructorArguments: args,
        });
    } catch (e : any) {
        if (e.message.toLowerCase().includes("already verified")) {
            console.log("Already Verified!")
        }
    }
}