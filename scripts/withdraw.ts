import { getNamedAccounts, ethers } from "hardhat"

async function main() {
  const { deployer } = await getNamedAccounts()
  const fundMe = await ethers.getContract("FundMe", deployer)
  console.log(`Got contract FundMe at ${fundMe.address}`)
  const transactionResponse = await fundMe.withdraw()
  await transactionResponse.wait()
  console.log("Withdrawed successfully!")
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.log(error)
    process.exit(1)
  })
