import { deployments, ethers, getNamedAccounts, network } from "hardhat"
import { assert, expect } from "chai"
import { FundMe, MockV3Aggregator } from "../../typechain-types"
import { developmentChains } from "../../helper-hardhat-config"

!developmentChains.includes(network.name)
  ? describe.skip
  : describe("FundMe", async () => {
      let fundMeContract: FundMe
      let deployer: string
      let mockV3Aggregator: MockV3Aggregator
      const fundValue = ethers.utils.parseEther("1") //1 ETH
      beforeEach(async () => {
        deployer = (await getNamedAccounts()).deployer
        await deployments.fixture(["all"])
        fundMeContract = await ethers.getContract("FundMe", deployer)
        mockV3Aggregator = await ethers.getContract(
          "MockV3Aggregator",
          deployer
        )
      })
      describe("constructor", async () => {
        it("sets the aggregator address correcttly", async () => {
          const response = await fundMeContract.getPriceFeed()
          assert.equal(response, mockV3Aggregator.address)
        })
      })

      describe("fundMethod", async () => {
        it("Fails if you don't send enough ETH", async () => {
          await expect(fundMeContract.fund()).to.be.revertedWith(
            "You need to spend more ETH!"
          )
        })

        it("updates the amount funded", async () => {
          await fundMeContract.fund({ value: fundValue })
          const response = await fundMeContract.getAddressToAmmountFunded(
            deployer
          )
          assert.equal(response.toString(), fundValue.toString())
        })

        it("add funder to array of funders", async () => {
          await fundMeContract.fund({ value: fundValue })
          const funder = await fundMeContract.getFunder(0)
          assert.equal(funder, deployer)
        })
      })

      describe("withdraw function", async () => {
        beforeEach(async () => {
          await fundMeContract.fund({ value: fundValue })
        })
        it("withdraw from single funder", async () => {
          //arrange
          const startingfundMeBalance =
            await fundMeContract.provider.getBalance(fundMeContract.address)
          const startingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //act
          const transaction = await fundMeContract.withdraw()
          const transactionReceipt = await transaction.wait(1)
          const { gasUsed, effectiveGasPrice } = transactionReceipt
          const gasCost = gasUsed.mul(effectiveGasPrice)

          const endingFundMeBalance = await fundMeContract.provider.getBalance(
            fundMeContract.address
          )
          const endingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //assert
          assert.equal(endingFundMeBalance.toString(), "0")
          assert.equal(
            startingfundMeBalance.add(startingBalanceDeployer).toString(),
            endingBalanceDeployer.add(gasCost).toString()
          )
        })

        it("cheaperWithdraw from single funder", async () => {
          //arrange
          const startingfundMeBalance =
            await fundMeContract.provider.getBalance(fundMeContract.address)
          const startingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //act
          const transaction = await fundMeContract.cheaperWithdraw()
          const transactionReceipt = await transaction.wait(1)
          const { gasUsed, effectiveGasPrice } = transactionReceipt
          const gasCost = gasUsed.mul(effectiveGasPrice)

          const endingFundMeBalance = await fundMeContract.provider.getBalance(
            fundMeContract.address
          )
          const endingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //assert
          assert.equal(endingFundMeBalance.toString(), "0")
          assert.equal(
            startingfundMeBalance.add(startingBalanceDeployer).toString(),
            endingBalanceDeployer.add(gasCost).toString()
          )
        })

        it("allow withdraw from multiple funders", async () => {
          //arrange
          const accounts = await ethers.getSigners()
          for (let i = 1; i < 6; i++) {
            const fundMeConnectedContract = await fundMeContract.connect(
              accounts[i]
            )
            await fundMeConnectedContract.fund({ value: fundValue })
          }
          const startingfundMeBalance =
            await fundMeContract.provider.getBalance(fundMeContract.address)
          const startingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //act
          const transaction = await fundMeContract.withdraw()
          const transactionReceipt = await transaction.wait(1)
          const { gasUsed, effectiveGasPrice } = transactionReceipt
          const gasCost = gasUsed.mul(effectiveGasPrice)

          const endingFundMeBalance = await fundMeContract.provider.getBalance(
            fundMeContract.address
          )
          const endingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //assert
          assert.equal(endingFundMeBalance.toString(), "0")
          assert.equal(
            startingfundMeBalance.add(startingBalanceDeployer).toString(),
            endingBalanceDeployer.add(gasCost).toString()
          )
          await expect(fundMeContract.getFunder(0)).to.be.reverted

          for (let i = 1; i < 6; i++) {
            await fundMeContract.getAddressToAmmountFunded(accounts[i].address),
              0
          }
        })

        it("only allow onwer to withdraw", async () => {
          //arrange
          const accounts = await ethers.getSigners()
          const notOwnerAddress = accounts[1]
          const fundMeConnectedContract = await fundMeContract.connect(
            notOwnerAddress
          )

          //act & assert
          await expect(fundMeConnectedContract.withdraw()).to.be.revertedWith(
            "FundMe__NotOwner"
          )
        })

        it("allow withdraw from multiple funders", async () => {
          //arrange
          const accounts = await ethers.getSigners()
          for (let i = 1; i < 6; i++) {
            const fundMeConnectedContract = await fundMeContract.connect(
              accounts[i]
            )
            await fundMeConnectedContract.fund({ value: fundValue })
          }
          const startingfundMeBalance =
            await fundMeContract.provider.getBalance(fundMeContract.address)
          const startingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //act
          const transaction = await fundMeContract.withdraw()
          const transactionReceipt = await transaction.wait(1)
          const { gasUsed, effectiveGasPrice } = transactionReceipt
          const gasCost = gasUsed.mul(effectiveGasPrice)

          const endingFundMeBalance = await fundMeContract.provider.getBalance(
            fundMeContract.address
          )
          const endingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //assert
          assert.equal(endingFundMeBalance.toString(), "0")
          assert.equal(
            startingfundMeBalance.add(startingBalanceDeployer).toString(),
            endingBalanceDeployer.add(gasCost).toString()
          )
          await expect(fundMeContract.getFunder(0)).to.be.reverted

          for (let i = 1; i < 6; i++) {
            await fundMeContract.getAddressToAmmountFunded(accounts[i].address),
              0
          }
        })

        it("allow cheaperwithdraw from multiple funders", async () => {
          //arrange
          const accounts = await ethers.getSigners()
          for (let i = 1; i < 6; i++) {
            const fundMeConnectedContract = await fundMeContract.connect(
              accounts[i]
            )
            await fundMeConnectedContract.fund({ value: fundValue })
          }
          const startingfundMeBalance =
            await fundMeContract.provider.getBalance(fundMeContract.address)
          const startingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //act
          const transaction = await fundMeContract.cheaperWithdraw()
          const transactionReceipt = await transaction.wait(1)
          const { gasUsed, effectiveGasPrice } = transactionReceipt
          const gasCost = gasUsed.mul(effectiveGasPrice)

          const endingFundMeBalance = await fundMeContract.provider.getBalance(
            fundMeContract.address
          )
          const endingBalanceDeployer =
            await fundMeContract.provider.getBalance(deployer)
          //assert
          assert.equal(endingFundMeBalance.toString(), "0")
          assert.equal(
            startingfundMeBalance.add(startingBalanceDeployer).toString(),
            endingBalanceDeployer.add(gasCost).toString()
          )
          await expect(fundMeContract.getFunder(0)).to.be.reverted

          for (let i = 1; i < 6; i++) {
            await fundMeContract.getAddressToAmmountFunded(accounts[i].address),
              0
          }
        })
      })
    })
