import { ethers, getNamedAccounts, network } from "hardhat"
import { assert } from "chai"
import { FundMe } from "../../typechain-types"
import { developmentChains } from "../../helper-hardhat-config"

developmentChains.includes(network.name)
  ? describe.skip
  : describe("FundMe", async () => {
      let fundMeContract: FundMe
      let deployer: string
      const fundValue = ethers.utils.parseEther("0.05")
      beforeEach(async () => {
        deployer = (await getNamedAccounts()).deployer
        fundMeContract = await ethers.getContract("FundMe", deployer)
      })

      it("allow people to fund and withdraw", async () => {
        //uses around 110k gas
        await fundMeContract.fund({ value: fundValue, gasLimit: 150000 })
        //uses around 40k gas
        await fundMeContract.withdraw({
          gasLimit: 100000,
        })
        const endingFundMeBalance = await fundMeContract.provider.getBalance(
          fundMeContract.address
        )
        assert.equal(endingFundMeBalance.toString(), "0")
      })
    })
