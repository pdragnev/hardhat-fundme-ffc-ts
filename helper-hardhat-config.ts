export interface networkConfigItem {
    ethUsdPriceFeedAddress?: string
    blockConfirmations?: number
    name ?: string
  }
  
  export interface networkConfigInfo {
    [chainId: number]: networkConfigItem
  }

export const networkConfig : networkConfigInfo = {
    5: {
        name: "goerli",
        ethUsdPriceFeedAddress: "0xD4a33860578De61DBAbDc8BFdb98FD742fA7028e",
        blockConfirmations: 6,
    }
}

export const developmentChains = ["hardhat","localhost"]

export const DECIMALS = 8
export const INITIAL_ANSWER = 200000000000